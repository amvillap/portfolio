## Welcome to my portfolio!

This site is meant to showcase my projects and share a little bit about myself and my
journey into programming!

## Design

I, initially, brainstormed what I would like to showcase on my portfolio. I decided to show:

- a little bit about me that also explains what I did prior to becoming a software engineer
- my projects (these are clickable links that can either take you to the application's deployed site or the gitlab for them!)
- ways to contact me!

## Easter egg

You didn't hear it from me, but if you click on Layla on the "contact" page, you might have the opportunity to give her a little treat! Don't drop it though!
