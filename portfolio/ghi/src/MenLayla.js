import menlayla from "./images/menlayla.png";
import "./Landing.css";

function MenLayla() {
    return (
        <>
            <div id="MenLayla" className="section">
                <img src={menlayla} alt="me and layla" />
            </div>
        </>
    )
}

export default MenLayla;
