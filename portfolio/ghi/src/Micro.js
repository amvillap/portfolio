function Micro() {
    return(
        <div id="Micro">
            <div className='micro-to-fullstack-container'>
                <div className="micro-to-fullstack">
                    <h4>from</h4>
                    <h1>micro</h1>
                    <h4>to</h4>
                    <h1>FULL STACK</h1>
                </div>
                <div className="micro-to-fullstack-text">
                    <p>
                    I, originally, graduated with a degree in Neurology, Physiology, and Behavior with an
                    extensive research/laboratory background. After almost three years of working in the food
                    safety testing industry, I felt unfulfilled and started looking into new skills that I can
                    learn. I then started to learn to code using free online resources and, eventually, I
                    joined HackReactor! Now, with my newfound programming skills it is my goal to eventually work
                     with Laboratory Information Management Systems - LIMS which would be a beautiful blend of
                     both of my backgrounds.
                    </p>
                </div>
            </div>
        </div>
    )
}

export default Micro;
