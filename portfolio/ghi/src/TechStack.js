import html from './images/htmllogo.png';
import js from './images/js.png';
import mongodb from './images/mongodblogo.png';
import postgre from './images/postgrelogo.png';
import python from './images/pythonlogo.png';
import react from './images/reactlogo.png';
import css from './images/csslogo.png';
import django from './images/djangologo.png';
import fastapi from './images/fastapi.svg';
import "./TechStack.css"

function TechStack() {
    return (
        <div id="TechStack">
                <div className='icons'>
                    <img
                        src={html}
                        alt="html"
                        width='200'
                        height='200'
                        className='icon icon1'
                    />
                    <img
                        src={js}
                        alt="js"
                        className='icon icon2'
                    />
                    <img
                        src={mongodb}
                        alt="mongodb"
                        className='icon icon3'
                    />
                    <img
                        src={postgre}
                        alt="postgre"
                        className='icon icon4'
                    />
                    <img
                        src={python}
                        alt="python"
                        className='icon icon5'
                    />
                    <img
                        src={react}
                        alt="react"
                        className='icon icon6'
                    />
                    <img
                        src={css}
                        alt="css"
                        className='icon icon7'
                    />
                    <img
                        src={django}
                        alt="django"
                        className='icon icon8'
                    />
                    <img
                        src={fastapi}
                        alt="fastapi"
                        className='icon icon9'
                    />
                </div>
            </div>
    )
}

export default TechStack;
