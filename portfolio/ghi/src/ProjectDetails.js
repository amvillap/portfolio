// import GabrielClements from "./images/Screenshot 2024-08-14 at 4.03.25 PM.png";
import "./ProjectDetails.css";

const projects = [
    {
        title: 'Gabriel Clements',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        technologies: 'blah blah blah',
    }
]


function ProjectDetails() {
    return(
        <>
            <div id="ProjectDetails">
                {/* <img className="image"src={projects[0].image} alt="projects title" /> */}
                <div className="right-container">
                    <div>{projects[0].title}</div>
                    <div>{projects[0].description}</div>
                    <div>
                        Technologies used:
                        <br></br>
                        {projects[0].technologies}
                    </div>
                </div>
            </div>
        </>
    )
};

export default ProjectDetails;
